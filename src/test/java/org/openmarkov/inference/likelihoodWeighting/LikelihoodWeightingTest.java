/*
 * Copyright 2011 CISIAD, UNED, Spain Licensed under the European Union Public
 * Licence, version 1.1 (EUPL) Unless required by applicable law, this code is
 * distributed on an "AS IS" basis, WITHOUT WARRANTIES OF ANY KIND.
 */

package org.openmarkov.inference.likelihoodWeighting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openmarkov.core.exception.IncompatibleEvidenceException;
import org.openmarkov.core.exception.InvalidStateException;
import org.openmarkov.core.exception.NotEvaluableNetworkException;
import org.openmarkov.core.exception.NodeNotFoundException;
import org.openmarkov.core.exception.UnexpectedInferenceException;
import org.openmarkov.core.inference.InferenceAlgorithm;
import org.openmarkov.core.inference.InferenceAlgorithmBNTest;
import org.openmarkov.core.model.network.EvidenceCase;
import org.openmarkov.core.model.network.Finding;
import org.openmarkov.core.model.network.NodeType;
import org.openmarkov.core.model.network.ProbNet;
import org.openmarkov.core.model.network.ProbNetOperations;
import org.openmarkov.core.model.network.Variable;
import org.openmarkov.core.model.network.potential.PotentialRole;
import org.openmarkov.core.model.network.potential.TablePotential;
import org.openmarkov.core.model.network.potential.UniformPotential;
import org.openmarkov.core.model.network.potential.canonical.TuningPotential;
import org.openmarkov.inference.variableElimination.VariableElimination;

/**
 * @author ibermejo
 */
public class LikelihoodWeightingTest extends InferenceAlgorithmBNTest
{
    private ProbNet             net;
    private LikelihoodWeighting lwAlgorithm;
    private ArrayList<Variable> variables;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp ()
        throws Exception
    {
        net = buildTestNet ();
        maxError = 2E-2;
    }

    @Test
    public void testTopologicalSorting ()
        throws Exception
    {
        lwAlgorithm = new LikelihoodWeighting (net);
        lwAlgorithm.setSampleSize (100000);
        net.addLink (net.getNode ("C"), net.getNode ("B"), true);
        List<Variable> variablesToSort = new ArrayList<Variable> ();
        variablesToSort.add (net.getVariable ("B"));
        variablesToSort.add (net.getVariable ("C"));
        variablesToSort.add (net.getVariable ("D"));
        List<Variable> sortedVariables = ProbNetOperations.sortTopologically (net, variablesToSort);
        Assert.assertEquals (sortedVariables.get (0), net.getVariable ("C"));
        Assert.assertEquals (sortedVariables.get (1), net.getVariable ("B"));
        Assert.assertEquals (sortedVariables.get (2), net.getVariable ("D"));
    }

    @Test
    public void testSampling ()
        throws Exception
    {
        lwAlgorithm = new LikelihoodWeighting (net);
        List<Finding> findings = new ArrayList<Finding>();
        // Evidence: B = B1
        findings.add (new Finding (variables.get (1), 1));
        EvidenceCase evidence = new EvidenceCase(findings);
        lwAlgorithm.setPostResolutionEvidence (evidence);
        HashMap<Variable, TablePotential> lwInferenceResult = lwAlgorithm.getProbsAndUtilities ();
        VariableElimination varEliminationAlgorithm = new VariableElimination (net);
        varEliminationAlgorithm.setPostResolutionEvidence (evidence);
        HashMap<Variable, TablePotential> veInferenceResult = varEliminationAlgorithm.getProbsAndUtilities ();
        for (Variable variable : variables)
        {
            Assert.assertArrayEquals (((TablePotential) veInferenceResult.get (variable)).values,
                                      ((TablePotential) lwInferenceResult.get (variable)).values,
                                      maxError);
        }
    }

    @Test
    public void testICISampling ()
        throws Exception
    {
        ProbNet testNet = buildTuningTestNet ();
        List<Finding> findings = new ArrayList<Finding> ();
        // Evidence: X2 = decreased
        findings.add ( new Finding (variables.get (1), 2));
        EvidenceCase evidence = new EvidenceCase(findings);
        lwAlgorithm = new LikelihoodWeighting (testNet);
        lwAlgorithm.setPostResolutionEvidence (evidence);
        Map<Variable, TablePotential> lwInferenceResult = lwAlgorithm.getProbsAndUtilities ();
        VariableElimination varEliminationAlgorithm = new VariableElimination (testNet);
        varEliminationAlgorithm.setPostResolutionEvidence (evidence);
        Map<Variable, TablePotential> veInferenceResult = varEliminationAlgorithm.getProbsAndUtilities ();
        for (Variable variable : variables)
        {
            Assert.assertArrayEquals (((TablePotential) veInferenceResult.get (variable)).values,
                                      ((TablePotential) lwInferenceResult.get (variable)).values,
                                      maxError);
        }
    }

    private ProbNet buildTestNet ()
        throws Exception
    {
        ProbNet testNet = new ProbNet ();
        variables = new ArrayList<Variable> ();
        variables.add (new Variable ("A", 2));
        variables.add (new Variable ("B", 2));
        variables.add (new Variable ("C", 2));
        variables.add (new Variable ("D", 2));
        variables.add (new Variable ("E", 2));
        for (Variable variable : variables)
        {
            testNet.addNode (variable, NodeType.CHANCE);
        }
        testNet.addLink (testNet.getNode ("A"), testNet.getNode ("B"), true);
        testNet.addLink (testNet.getNode ("A"), testNet.getNode ("C"), true);
        testNet.addLink (testNet.getNode ("B"), testNet.getNode ("D"), true);
        testNet.addLink (testNet.getNode ("B"), testNet.getNode ("E"), true);
        testNet.addLink (testNet.getNode ("C"), testNet.getNode ("D"), true);
        // A
        UniformPotential aPotential = new UniformPotential (PotentialRole.JOINT_PROBABILITY,
                                                            variables.get (0));
        testNet.addPotential (aPotential);
        // A -> B
        TablePotential abPotential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (1), variables.get (0));
        abPotential.values = new double[] {0.7, 0.3, 0.2, 0.8};
        testNet.addPotential (abPotential);
        // A -> C
        TablePotential acPotential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (2), variables.get (0));
        acPotential.values = new double[] {0.6, 0.4, 0.25, 0.75};
        testNet.addPotential (acPotential);
        // B -> D
        TablePotential bcdPotential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                          variables.get (3), variables.get (1),
                                                          variables.get (2));
        bcdPotential.values = new double[] {0.5, 0.5, 0.5, 0.5, 0.7, 0.3, 0.2, 0.8};
        testNet.addPotential (bcdPotential);
        // C -> E
        TablePotential cePotential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (4), variables.get (2));
        cePotential.values = new double[] {0.6, 0.4, 0.25, 0.75};
        testNet.addPotential (cePotential);
        return testNet;
    }

    private ProbNet buildTuningTestNet ()
        throws Exception
    {
        ProbNet testNet = new ProbNet ();
        variables = new ArrayList<Variable> ();
        variables.add (new Variable ("X1", 3));// 0
        variables.add (new Variable ("X2", 3));// 1
        variables.add (new Variable ("X3", 3));// 2
        variables.add (new Variable ("C", 3));// 3
        variables.add (new Variable ("A1", 3));// 4
        variables.add (new Variable ("Y", 3));// 5
        for (Variable variable : variables)
        {
            testNet.addNode (variable, NodeType.CHANCE);
        }
        testNet.addLink (testNet.getNode ("C"), testNet.getNode ("A1"), true);
        testNet.addLink (testNet.getNode ("X1"), testNet.getNode ("A1"), true);
        testNet.addLink (testNet.getNode ("A1"), testNet.getNode ("Y"), true);
        testNet.addLink (testNet.getNode ("X2"), testNet.getNode ("Y"), true);
        testNet.addLink (testNet.getNode ("X3"), testNet.getNode ("Y"), true);
        // X1
        TablePotential x1Potential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (0));
        x1Potential.values = new double[] {0.1, 0.7, 0.2};
        testNet.addPotential (x1Potential);
        // X2
        TablePotential x2Potential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (1));
        x2Potential.values = new double[] {0.3, 0.6, 0.1};
        testNet.addPotential (x2Potential);
        // X3
        TablePotential x3Potential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                         variables.get (2));
        x3Potential.values = new double[] {0.1, 0.8, 0.1};
        testNet.addPotential (x3Potential);
        // C
        TablePotential cPotential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                        variables.get (3));
        cPotential.values = new double[] {0.3, 0.4, 0.3};
        testNet.addPotential (cPotential);
        // C, X1 -> A1
        TablePotential cX1A1Potential = new TablePotential (PotentialRole.CONDITIONAL_PROBABILITY,
                                                            variables.get (4), variables.get (3),
                                                            variables.get (0));
        cX1A1Potential.values = new double[] {0.9, 0.1, 0.0, 0.0, 1.0, 0.0, 0.0, 0.1, 0.9, 0.6,
                0.4, 0.0, 0.0, 1.0, 0.0, 0.0, 0.6, 0.4, 0.28, 0.68, 0.04, 0.0, 1.0, 0.0, 0.07,
                0.67, 0.26};
        testNet.addPotential (cX1A1Potential);
        // A1, X2, X3 -> Y
        TuningPotential tuningPotential = new TuningPotential (variables.get (5),
                                                                         variables.get (4),
                                                                         variables.get (1),
                                                                         variables.get (2));
        // Noisy A1
        tuningPotential.setNoisyParameters (variables.get (4), new double[] {0.9, 0.1, 0.0, 0.0,
                1.0, 0.0, 0.0, 0.1, 0.9});
        // Noisy X2
        tuningPotential.setNoisyParameters (variables.get (1), new double[] {0.95, 0.05, 0.0, 0.0,
                1.0, 0.0, 0.0, 0.05, 0.95});
        // Noisy X3
        tuningPotential.setNoisyParameters (variables.get (2), new double[] {0.92, 0.08, 0.0, 0.0,
                1.0, 0.0, 0.0, 0.08, 0.92});
        testNet.addPotential (tuningPotential);
        return testNet;
    }
    
    /**
     * @throws NodeNotFoundException
     * Tests the a priori joint probabilities obtained in the network Asia
     * @throws UnexpectedInferenceException 
     * @throws IncompatibleEvidenceException 
      * @throws NotEvaluableNetworkException 
     */
    @Test
    public void testAPosterioriFamilyJointProbabilitiesBN_Asia ()
        throws NodeNotFoundException,
        IncompatibleEvidenceException,
        UnexpectedInferenceException, NotEvaluableNetworkException
    {
        ProbNet network = bN_Asia;
        LikelihoodWeighting inferenceAlgorithm =  new LikelihoodWeighting (network);
        inferenceAlgorithm.setSampleSize (100000);
        EvidenceCase evidence = new EvidenceCase ();
        try
        {
            // T=absent, TOrC=yes
            evidence.addFinding (network, "T", "absent");
            evidence.addFinding (network, "TOrC", "yes");
        }
        catch (InvalidStateException | IncompatibleEvidenceException e1)
        {
            e1.printStackTrace ();
        }
        
        inferenceAlgorithm.setPreResolutionEvidence (evidence);
        ArrayList<Variable> variables = new ArrayList<> ();
        variables.add (network.getVariable ("T"));
        variables.add (network.getVariable ("TOrC"));
        variables.add (network.getVariable ("L"));
        
        Map<Variable, TablePotential> jointProbabilities = inferenceAlgorithm.getFamilyJointProbabilities ();
    }    

    @Override
    public InferenceAlgorithm buildInferenceAlgorithm (ProbNet probNet)
        throws NotEvaluableNetworkException
    {
        LikelihoodWeighting algorithm = new LikelihoodWeighting (probNet);
        algorithm.setSampleSize (100000);
        return algorithm;
    }
}
